#ifndef _INCLUDE_GUARD_FIELD_
#define _INCLUDE_GUARD_FIELD_
#include "Field.h"
#endif
#ifndef _INCLUDE_GUARD_FIGURE_
#define _INCLUDE_GUARD_FIGURE_
#include "Figure.h"
#endif
#ifndef _INCLUDE_GUARD_IOSTREAM_
#define _INCLUDE_GUARD_IOSTREAM_
#include <iostream>
#endif
#ifndef _INCLUDE_GUARD_WALKER_
#define _INCLUDE_GUARD_WALKER_
#include "NewWalker.h"
#endif
#ifndef _INCLUDE_GUARD_FSTREAM_
#define _INCLUDE_GUARD_FSTREAM_
#include <fstream>
#endif
#ifndef _INCLUDE_GUARD_UTILS_
#define _INCLUDE_GUARD_UTILS_
#include "Utils.h"
#endif


using namespace std;


int main() 
{
	// Gebe leeres Beispielfeld aus
	cout << print_field(nullptr, 0) << endl;

	// Frage Startfeld ab
	knight_input:
	cout << "Bitte geben Sie das Startfeld des Pferdes ein. Z.B.: H8" << endl << "Feld: ";
	char* entry = new char[2];
	std::cin >> entry;

	char x = entry[0];
	int y = entry[1] - '0';


	// Wandle Buchstaben in Zahl f�r interne Logik um
	x = convert_to_logik_value(x);

	// Da auf dem Brett bei 1 angefangen wird zu Z�hlen
	y--;
	
	// Pr�fe auf falsche Eingabe
	if (x < 0 || x > 7 || y < 0 || y > 7)
	{
		cout << "Falsche Eingabe!" << endl;
		goto knight_input;
	}
	// Bildschirm l�schen
	system("cls");

	// Erzeugt einen Springer, auf Ausgangsposition
	knight k1;

	k1.pos.x = x;
	k1.pos.y = y;

	// Gebe Feld mit Startposition aus
	cout << print_field(&k1.pos, 1) << endl;

	// Frage Threadverz�gerung ab
	thread_sleep_millis_input:
	cout << "Bitte geben Sie an, wie viele Millisekunden Sie den Programmablauf verzoegern moechten (0 - 10000)" << endl << "t: ";
	int thread_sleep_millis;
	cin >> thread_sleep_millis;
	// Pr�fe auf falsche Eingabe
	if (thread_sleep_millis < 0 || thread_sleep_millis > 10000)
	{
		cout << "Falsche Eingabe!" << endl;
		goto thread_sleep_millis_input;
	}

	// Bildschirm l�schen
	system("cls");

	// Legt ein Array mit allen m�glichen Feldern an
	field* f = new field[64];

	f[0].x = k1.pos.x;
	f[0].y = k1.pos.y;

	// Anzahl der m�glichen Felder, ist nur das Startfeld
	int amount_visited_fields = 1;


	// Renne
	if (walk(&k1, f, &amount_visited_fields, thread_sleep_millis))
	{
		cout << print_field(f, amount_visited_fields) << endl;


		// Gebe die Z�ge im Verzeichniss der EXE, oder des Projektpfades in der TXT Datei 
		// springer_zuege.txt aus.
		fstream fstr;
		fstr.open("springer_zuege.txt", ios::out);

		fstr << convert_to_chess_field(f[0].x) << f[0].y + 1 << endl;

		for (int i = 1; i < 64; i++)
		{
			fstr << convert_to_chess_field(f[i].x) << f[i].y + 1 << endl;
		}
		fstr.close();
	}


	cout << "Dankeschoen!" << endl << "Die Datei mit den Zuegen befindet sich im Pfad der Programm Datei." << endl;
	int stop;
	cin >> stop;
	
	
}