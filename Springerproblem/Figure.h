/*.
Springer besitzt Position
*/
struct knight 
{
	field pos;
};
/*
Gibt zur�ck ob ein Sprung m�glich oder nicht m�glich ist.
*/
bool valide_knight_jump(knight knight, field end_field, field* visited_fields, int amount_visited_fields);
/*
M�gliche Spr�nge die eine Figur ausf�hren kann.
*/
jump_fields get_possible_knight_jumps(knight knight, field* visited_fields, int amount_visited_fields);
/*
F�hrt den Sprung der Figur aus. Pr�ft auf korrekten Sprung. Gibt ein Array mit bereits besuchten Feldern zur�ck.
*/
bool knight_jump(knight* knight, field end_field, field* visited_fields, int* amount_visited_fields);
/*
M�gliche verbesserung der Laufzeit des Backtracking Algorithmus.
*/
jump_fields wandsdorf_sort(jump_fields jmp_f, field* visited_fields, int* amount_visited_fields);