#ifndef _INCLUDE_GUARD_STRING_
#define _INCLUDE_GUARD_STRING_
#include <string>
#endif
#ifndef _INCLUDE_GUARD_UTILS_
#define _INCLUDE_GUARD_UTILS_
#include "Utils.h"
#endif

int convert_to_logik_value(char c)
{

	switch (c)
	{
	case 'a':
		return 0;
		break;
	case 'A':
		return 0;
		break;
	case 'b':
		return 1;
		break;
	case 'B':
		return 1;
		break;
	case 'c':
		return 2;
		break;
	case 'C':
		return 2;
		break;
	case 'd':
		return 3;
		break;
	case 'D':
		return 3;
		break;
	case 'e':
		return 4;
		break;
	case 'E':
		return 4;
		break;
	case 'f':
		return 5;
		break;
	case 'F':
		return 5;
		break;
	case 'g':
		return 6;
		break;
	case 'G':
		return 6;
		break;
	case 'h':
		return 7;
		break;
	case 'H':
		return 7;
		break;
	default:
		return -1;
	}
}

std::string convert_to_chess_field(int i)
{

	switch (i)
	{
	case 0:
		return "A";
		break;
	case 1:
		return "B";
		break;
	case 2:
		return "C";
		break;
	case 3:
		return "D";
		break;
	case 4:
		return "E";
		break;
	case 5:
		return "F";
		break;
	case 6:
		return "G";
		break;
	case 7:
		return "H";
		break;
	default:
		return "#";
	}
}