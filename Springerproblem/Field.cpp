#ifndef _INCLUDE_GUARD_STRING_
#define _INCLUDE_GUARD_STRING_
#include <string>
#endif
#ifndef _INCLUDE_GUARD_IOSTREAM_
#define _INCLUDE_GUARD_IOSTREAM_
#include <iostream>
#endif
#ifndef _INCLUDE_GUARD_FIELD_
#define _INCLUDE_GUARD_FIELD_
#include "Field.h"
#endif

using namespace std;

string print_field(field* fields, int array_size)
{
	string result = "";
	
	// Wand darstellung des Schachbretts.
	const string wall = "#################################\n";
	const string x_numbers = "  A   B   C   D   E   F   G   H\n";
	const string wall_tag = "#";
	const string knight_tag = ">-<";
	const string visited_field_tag = "(X)";
	const string empty_field = "   ";
	const string new_line = "\n";

	// Ausgabe des Schachbrettes in y Richtung.
	for (int i = 0; i < 8; i++)
	{
		result += wall;

		// Ausgabe des Schachbrettes in x Richtung.
		for (int y = 0; y < 9; y++)
		{
			// Pr�fe, ob auf Feld ein Springer steht.
			for (int j = 0; j < array_size; j++)
			{
				if (fields[j].x == y && fields[j].y == i)
				{
					if (fields[array_size - 1].x == y && fields[array_size - 1].y == i)
					{
						// Setze Springer
						result += wall_tag + knight_tag;
						// Bricht Schleife ab und verhindere, dass ein leeres Feld ausgegeben wird.
						goto found_visited_field;
					}
					else
					{
						// Setze belegtes Feld
						result += wall_tag + visited_field_tag;
						// Z�hle Laufvariable f�r x Richtung um eins hoch, um keine �berfl�ssigen W�nde zu ziehen.
						goto found_visited_field;
					}
					
				}

			}
			// Gebe leeres Feld aus.
			result += wall_tag + empty_field;
			
			// Mache weiter mit Ausgabe des Feldes.
			found_visited_field:
			;

			
			
		}
		// Gebe Zeilennummern und Zeilenumbruch aus.
		result += std::to_string(i+1);
		result += new_line;
		
	}
	result += wall;

	// Gebe Spaltennummern aus.
	result += new_line;
	result += x_numbers;

	return result;
}