#ifndef _INCLUDE_GUARD_STRING_
#define _INCLUDE_GUARD_STRING_
#include <string>
#endif
/*
Einzelnes Feld und x und y Koordinaten.
*/
struct field 
{
	int x;
	int y;
};
/*
Felder, auf die eine Figur springen kann.
*/
struct jump_fields
{
	int field_amount;
	field* fields;
};
/*
Schachbrett in 8 x 8 Feldern.
*/
std::string print_field(field*, int array_size);