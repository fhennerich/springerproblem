#ifndef _INCLUDE_GUARD_FIELD_
#define _INCLUDE_GUARD_FIELD_
#include "Field.h"
#endif
#ifndef _INCLUDE_GUARD_FIGURE_
#define _INCLUDE_GUARD_FIGURE_
#include "Figure.h"
#endif
#ifndef _INCLUDE_GUARD_MATH_
#define _INCLUDE_GUARD_MATH_
#include <math.h>
#endif

/*
Es wird versucht die Laufzeit des Backtracking Algorithmus zu verbessern, indem immer das Feld als n�chsten Schritt gew�lt wird,
welches am wenigsten m�gliche Anschlussfelder besitzt.
*/
jump_fields wandsdorf_sort(jump_fields jmp_f, field* visited_fields, int* amount_visited_fields)
{
	// Wenn Felder vorhanden
	if (jmp_f.field_amount > 0)
	{

		// Bullbesort der Felder
		field f;
		for (int i = 1; i < jmp_f.field_amount; i++)
		{
			for (int j = 0; j < jmp_f.field_amount - i; j++)
			{
				knight k1;
				k1.pos = jmp_f.fields[j];
				field* new_visited_fields_k1 = new field[*amount_visited_fields + 1];
				for (int y = 0; y < *amount_visited_fields; y++)
				{
					new_visited_fields_k1[y] = visited_fields[y];
				}
				new_visited_fields_k1[*amount_visited_fields] = k1.pos;
				// Anzahl der M�glichkeiten der Anschlussfelder f�r k1
				int amount1 = get_possible_knight_jumps(k1, new_visited_fields_k1, *amount_visited_fields + 1).field_amount;
				delete[] new_visited_fields_k1;
				knight k2;
				k2.pos = jmp_f.fields[j + 1];
				field* new_visited_fields_k2 = new field[*amount_visited_fields + 1];
				for (int y = 0; y < *amount_visited_fields; y++)
				{
					new_visited_fields_k2[y] = visited_fields[y];
				}
				new_visited_fields_k2[*amount_visited_fields] = k2.pos;
				// Anzahl der M�glichkeiten der Anschlussfelder f�r k2
				int amount2 = get_possible_knight_jumps(k2, new_visited_fields_k2, *amount_visited_fields + 1).field_amount;
				delete[] new_visited_fields_k2;
				// Sortiere
				if (amount1>amount2)
				{
					f = jmp_f.fields[j];
					jmp_f.fields[j] = jmp_f.fields[j + 1];
					jmp_f.fields[j + 1] = f;

				}
			}
		}

		return jmp_f;
	}
	else
	{
		return jmp_f;
	}
}

jump_fields get_possible_knight_jumps(knight knight, field* visited_fields, int amount_visited_fields)
{
	field f;

	field* possible_fields = new field[8];
	// Anzahl der m�glichen zu besuchenden Felder
	int amount_possible_fields = 0;

	// F�r alle Felder eines Schachbretts
	for (int i = 0; i < 8; i++)
	{
		for (int y = 0; y < 8; y++)
		{
			f.x = i;
			f.y = y;

			// Ist ein Feld g��tig, f�ge is zum Array hinzu
			if (valide_knight_jump(knight, f, visited_fields, amount_visited_fields))
			{
				possible_fields[amount_possible_fields] = f;
				amount_possible_fields++;
			}
		}
	}
	

	
	// Gebe m�gliche Spr�nge in Form von Array und Anzahl zur�ck
	jump_fields jmp_f;
	jmp_f.fields = &possible_fields[0];
	jmp_f.field_amount = amount_possible_fields;


	return jmp_f;
}

bool valide_knight_jump(knight knight, field end_field, field* visited_fields, int amount_visited_fields)
{
	bool result = false;

	// Pr�fe ob Zielfeld ein bereits besuchtes Feld ist.
	for (int i = 0; i < amount_visited_fields; i++)
	{
		if ((visited_fields[i].x == end_field.x) && (visited_fields[i].y == end_field.y))
		{
			return false;
		}
	}
	// Pr�fe ob Zielfeld einem g�ltigen Zug des Springers entspricht.
	if (((abs(knight.pos.x - end_field.x) == 2 && abs(knight.pos.y - end_field.y) == 1)
		|| (abs(knight.pos.x - end_field.x) == 1 && abs(knight.pos.y - end_field.y) == 2))
		&& ((knight.pos.x >= 0 && knight.pos.x < 8) && (knight.pos.y >= 0 && knight.pos.y < 8)
		&& (end_field.x >= 0 && end_field.x < 8) && (end_field.y >= 0 && end_field.y < 8)))
	{
		result = true;
	}
	
	return result;
	
}



bool knight_jump(knight* knight, field end_field, field* visited_fields, int* amount_visited_fields)
{
	// F�hre Sprung aus. falls Zielfeld g�ltig.
	if (valide_knight_jump(*knight, end_field, visited_fields, *amount_visited_fields))
	{
		// F�gt neues Feld zu den bereits besuchten Feldern hinzu
		visited_fields[*amount_visited_fields] = end_field;
		// Erh�ht Anzahl, der besuchten Felder um Eins
		*amount_visited_fields += 1;
		knight[0].pos.x = end_field.x;
		knight[0].pos.y = end_field.y;
		return true;
				
	}
		
	return false;
}