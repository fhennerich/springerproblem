#ifndef _INCLUDE_GUARD_FIELD_
#define _INCLUDE_GUARD_FIELD_
#include "Field.h"
#endif
#ifndef _INCLUDE_GUARD_FIGURE_
#define _INCLUDE_GUARD_FIGURE_
#include "Figure.h"
#endif
#ifndef _INCLUDE_GUARD_IOSTREAM_
#define _INCLUDE_GUARD_IOSTREAM_
#include <iostream>
#endif
#ifndef _INCLUDE_GUARD_WALKER_
#define _INCLUDE_GUARD_WALKER_
#include "NewWalker.h"
#endif
#ifndef _INCLUDE_GUARD_THREAD_
#define _INCLUDE_GUARD_THREAD_
#include <thread>
#endif
#ifndef _INCLUDE_GUARD_CHRONO_
#define _INCLUDE_GUARD_CHRONO_
#include <chrono>
#endif
#ifndef _INCLUDE_GUARD_STDLIB_
#define _INCLUDE_GUARD_STDLIB_
#include <stdlib.h>
#endif




bool walk(knight* k, field* visited_fields, int* amount_visited_fields, int thread_sleep_millis)
{
	// Gebe aktuellen Stand des Schachbrettes aus
	std::cout << print_field(visited_fields, *amount_visited_fields) << std::endl;
	// Verz�gerung des Ablaufs der Animation
	std::this_thread::sleep_for(std::chrono::milliseconds(thread_sleep_millis));
	// Bildschirm l�schen
	system("cls");

	// Wenn genaue Anzahl an Z�gen erreicht und der Algorithmus terminieren soll
	if (*amount_visited_fields == 64)
	{
		return true;
	}

	// Hole verf�gbare Spr�nge
	jump_fields jmp_f = get_possible_knight_jumps(k[0], visited_fields, *amount_visited_fields);

	// Versuch die laufzeit des Backtracking Algorithmus zu verbessern

	jmp_f = wandsdorf_sort(jmp_f, visited_fields, amount_visited_fields);

	// F�r alle m�glichen Spr�nge
	for (int i = 0; i < jmp_f.field_amount; i++)
	{
		// F�hre Sprung aus
		knight_jump(k, jmp_f.fields[i], visited_fields, amount_visited_fields);
		
		// Wenn Algorithmus terminiert, schleife true druch
		if (walk(k, visited_fields, amount_visited_fields, thread_sleep_millis))
		{
			return true;
		}
		// Sind keine Felder mehr verf�gbar, springe ein Feld zur�ck und setze altes Feld auf noch nicht besucht
		else {
			
			*amount_visited_fields -= 2;

			// F�hre Sprung aus
			knight_jump(k, visited_fields[*amount_visited_fields], visited_fields, amount_visited_fields);

			// Gebe aktuellen Stand des Schachbrettes aus
			std::cout << print_field(visited_fields, *amount_visited_fields) << std::endl;
			// Verz�gerung des Ablaufs der Animation
			std::this_thread::sleep_for(std::chrono::milliseconds(thread_sleep_millis));
			// Bildschirm l�schen
			system("cls");
		}
		
	}
	// Sind keine Felder mehr verf�gbar, springe zur�ck
	return false;
}